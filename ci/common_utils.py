#!/usr/bin/env python3

import argparse
import os

from yaml import load, Loader

CWD = os.path.abspath(os.getcwd())

def get_meta_file(software, version):
    meta_path = os.path.join(CWD, 'tools', software, version, "meta.yml".format(software, version))
    return(load(open(meta_path, "r"), Loader=Loader))


def get_aliases(software, version):
    """Check if local src files are needed"""
    meta = get_meta_file(software, version)
    if len(meta.get('aliases', [])) > 0:
        print(" ".join(meta.get('aliases', [])))
    else:
        print(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--software', help="Software name")
    parser.add_argument('--version', help="Software version number")
    parser.add_argument('--function', choices=('is_infra_software_src_path', 'is_local_src_needed', 'is_local_src_exists','get_aliases'))
    args = parser.parse_args()
    eval(args.function)(args.software, args.version)
