BootStrap: docker
From: ubuntu:22.04

%labels
    Author IFB

%files
    gmes_linux_64_4.tar.gz /opt/gmes_linux_64_4.tar.gz
    get_augustus_config /usr/bin/

%post
    export BRAKER2_VERSION=2.1.6
    export PROHINT_VERSION=2.6.0

    ## From apt
    export DEBIAN_FRONTEND=noninteractive
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget tar locales
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*

    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

    ## Conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    rm Miniconda3-latest-Linux-x86_64.sh

    export PATH=/opt/conda/bin:$PATH
    conda update -q -y conda
    conda install -c conda-forge -y mamba

    ## Braker2
    mamba install -q -y -c conda-forge -c bioconda -c defaults bioconda::braker2=${BRAKER2_VERSION}

    ## GeneMark-ES
    cd /opt
    tar -zxf gmes_linux_64_4.tar.gz
    cd gmes_linux_64_4
    perl change_path_in_perl_scripts.pl "/usr/bin/env perl"
    mamba install -q -y -c conda-forge -c bioconda -c defaults \
      perl-yaml \
      perl-hash-merge \
      perl-parallel-forkmanager \
      perl-mce \
      perl-threaded \
      perl-math-utils  \

    ## ProtHint
    cd /opt
    wget -q https://github.com/gatech-genemark/ProtHint/archive/refs/tags/v${PROHINT_VERSION}.tar.gz
    tar -zxf v${PROHINT_VERSION}.tar.gz
    ln -s ProtHint-${PROHINT_VERSION}/ ProtHint

    # Cleaning
    rm -rf /opt/gmes_linux_64_4.tar.gz
    conda remove -y mamba
    conda clean -y --all

%environment
    export PATH=/opt/conda/bin:/opt/ProtHint/bin:$PATH
    export GENEMARK_PATH=/opt/gmes_linux_64_4/
    export LANG=en_US.utf8

%test
    export PATH=/opt/conda/bin:/opt/ProtHint/bin:$PATH
    export GENEMARK_PATH=/opt/gmes_linux_64_4/
    export LANG=en_US.utf8

    braker.pl --version

    # Test locally with a licence key
    # /opt/gmes_linux_64_4/check_install.bash

